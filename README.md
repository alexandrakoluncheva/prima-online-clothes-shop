# Prima - Online Clothes Shop

Spring WEB Application used for managing a clothes shop

Goals:

    - Fully working and easy to manage

    - Hosted on AWS
    
    - Amazon Cognito - for security and authentication
    
    - Google API's - Maps, Gmail
    
    - Currency converter
    
    - PDF Reciept Generator
    
    - (Optional) Set up Docker, Kubernetes and Jenkins
