create table brand
(
    id varchar(40) default uuid() not null
        primary key,
    name varchar(40) not null
);

create table category
(
    id varchar(40) default uuid() not null
        primary key,
    name varchar(40) not null
);

create table products
(
    product_id varchar(40) default uuid() not null
        primary key,
    size varchar(40) not null,
    price int not null,
    name varchar(40) null,
    category varchar(40) not null,
    quantity int null,
    enabled tinyint default 1 null,
    brand varchar(40) null,
    constraint products_brand_fk
        foreign key (brand) references brand (id),
    constraint products_category_fk
        foreign key (category) references category (id)
);

create table users
(
    username varchar(50) not null
        primary key,
    password varchar(68) not null,
    enabled tinyint default 1 not null
);

create table authorities
(
    username varchar(50) not null,
    authority varchar(50) not null,
    constraint username_authority
        unique (username, authority),
    constraint authorities_fk
        foreign key (username) references users (username)
);

create table user_details
(
    id varchar(40) default uuid() not null
        primary key,
    username varchar(60) not null,
    phone varchar(11) not null,
    address varchar(80) not null,
    first_name varchar(40) not null,
    last_name varchar(40) not null,
    enabled tinyint default 1 null,
    constraint user_details_phone_uindex
        unique (phone),
    constraint user_details_username_fk
        foreign key (username) references users (username)
);

create table `order`
(
    id varchar(40) default uuid() not null
        primary key,
    user_details varchar(40) not null,
    product varchar(40) not null,
    constraint order_product_fk
        foreign key (product) references products (product_id),
    constraint order_user_details_fk
        foreign key (user_details) references user_details (id)
);


