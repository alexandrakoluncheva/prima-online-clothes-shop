insert into prima.brand (id, name) values ('b07cc7a6-b357-11eb-b612-9829a64392c7', 'Gucci');
insert into prima.category (id, name) values ('b59b630f-b357-11eb-b612-9829a64392c7', 'Shirt');
insert into prima.users (username, password, enabled) values ('alexandrakoluncheva@gmail.com', '$2a$10$l1/E.31RdSeo0KPgSUeKzurBUNWVfJDg3h036oGOVH3/bPm4Ucnai', 1);
insert into prima.user_details (id, username, phone, address, first_name, last_name, enabled) values ('cb53712b-b357-11eb-b612-9829a64392c7', 'alexandrakoluncheva@gmail.com', '0894610711', 'Sofia', 'Alexandra', 'Koluncheva', 1);
insert into prima.authorities (username, authority) values ('alexandrakoluncheva@gmail.com', 'ROLE_USER');
insert into prima.products (product_id, size, price, name, category, quantity, enabled, brand) values ('e067121a-b357-11eb-b612-9829a64392c7', 's', 134, 'Gucci 123', 'b59b630f-b357-11eb-b612-9829a64392c7', 5, 1, 'b07cc7a6-b357-11eb-b612-9829a64392c7');
insert into prima.order (id, user_details, product) values ('1f148110-b358-11eb-b612-9829a64392c7', 'cb53712b-b357-11eb-b612-9829a64392c7', 'e067121a-b357-11eb-b612-9829a64392c7');
