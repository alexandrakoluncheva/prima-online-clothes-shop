package com.example.prima;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PrimaApplication {

    public static void main(String[] args){
        SpringApplication.run(PrimaApplication.class, args);
    }

}
