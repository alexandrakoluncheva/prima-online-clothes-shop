package com.example.prima.services.contracts;

import com.example.prima.models.User;

import javax.mail.MessagingException;
import java.io.IOException;
import java.security.GeneralSecurityException;

public interface UserService {

    void create(User user) throws GeneralSecurityException, IOException, MessagingException;

    User getByEmail(String email);

    User getByResetPasswordToken(String token);

    void updateResetPasswordToken(String token, String email);

    void updatePassword(User user, String newPassword);

}
