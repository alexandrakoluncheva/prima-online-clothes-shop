package com.example.prima.services;

import com.example.prima.models.User;
import com.example.prima.repositories.contracts.UserRepository;
import com.example.prima.services.contracts.UserService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import javax.persistence.EntityNotFoundException;
import java.io.IOException;
import java.security.GeneralSecurityException;

@Service
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;

    public UserServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public void create(User user)
            throws GeneralSecurityException, IOException, MessagingException {

        userRepository.create(user);
    }

    @Override
    public User getByEmail(String email) {
        return userRepository.getByEmail(email);
    }

    /**
     * Sets a new reset token when called
     */
    @Override
    public void updateResetPasswordToken(String token,
                                         String email) {

        User user = userRepository.getByEmail(email);

        if (user != null) {
            user.setResetPasswordToken(token);
            userRepository.update(user);
        } else {
            throw new EntityNotFoundException();
        }
    }

    /**
     * Gets the reset token
     */
    @Override
    public User getByResetPasswordToken(String token) {
        return userRepository.findByResetPasswordToken(token);
    }

    /**
     * Gets the new password, encodes it and sets it
     * Once the reset token has been used it will go back to null
     */
    @Override
    public void updatePassword(User user, String newPassword) {
        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        String encodedPassword = passwordEncoder.encode(newPassword);

        user.setPassword(encodedPassword);
        user.setResetPasswordToken(null);

        userRepository.update(user);
    }
}
