package com.example.prima.gmail;

import com.google.api.services.gmail.Gmail;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import com.google.api.client.util.Base64;
import com.google.api.services.gmail.Gmail;
import com.google.api.services.gmail.model.Message;


/**
 * Gmail Commands implemented so far:
 * - sendEmailWithHTMLBodyAndAttachment(receiver); - send email with HTML body attachment
 * - getMailBody("alexandrakoluncheva@gmail.com"); - reads the last email from a given email
 * - GmailOperations.sendEmail(receiver, subject, bodyText); - sends email with text only
 * - getGmailService(); get the gmail service tokens
 */

public class GmailOperations {

    public static final String user = "me";

    public static void sendMessage(Gmail service, String userId, MimeMessage email)
            throws MessagingException, IOException {

        Message message = createMessageWithEmail(email);
        message = service.users().messages().send(userId, message).execute();

        System.out.println("Message id: " + message.getId());
        System.out.println(message.toPrettyString());
    }

    // send message with text only:

    public static Message createMessageWithEmail(MimeMessage email)
            throws MessagingException, IOException {

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        email.writeTo(baos);
        String encodedEmail = Base64.encodeBase64URLSafeString(baos.toByteArray());
        Message message = new Message();
        message.setRaw(encodedEmail);
        return message;
    }

    public static MimeMessage createEmail(String to,
                                          String from,
                                          String subject,
                                          String bodyText) throws MessagingException, IOException {

        Properties props = new Properties();
        Session session = Session.getDefaultInstance(props, null);

        MimeMessage email = new MimeMessage(session);

        email.setFrom(new InternetAddress(from)); //me - prima.shop.bulgaria@gmail.com
        email.addRecipient(javax.mail.Message.RecipientType.TO, new InternetAddress(to)); //
        email.setSubject(subject);

        email.setText(bodyText);

        return email;
    }

    /**
     * Method to send a plain text email using Gmail api
     *
     * @param receiver the email you want to send the attachment to
     * @param subject  the subject of the email (title)
     * @param bodyText the text that will be shown in the body
     */
    public static void sendEmail(String receiver,
                                 String subject,
                                 String bodyText) throws IOException,
            GeneralSecurityException, MessagingException {

        Gmail service = GmailUtils.getGmailService();
        MimeMessage Mimemessage =
                createEmail(receiver,
                        "me",
                        subject,
                        bodyText);

        Message message = createMessageWithEmail(Mimemessage);

        message = service.users().messages().send("me", message).execute();

        System.out.println("Message id: " + message.getId());
        System.out.println("Receiver: " + receiver);
        System.out.println(message.toPrettyString());
    }

    /**
     * Method to create the email structure of email with body attachment
     *
     * @param to             email you want to send the Email to
     * @param subject        email subject (title)
     * @param html           type of attachment to send
     * @param htmlReportPath the full path to the attachment
     */

    public static MimeMessage createHTMLEmailBodyWithAttachment(String to,
                                                                String subject,
                                                                String html,
                                                                String htmlReportPath)
            throws AddressException, MessagingException {

        Properties props = new Properties();
        Session session = Session.getDefaultInstance(props, null);

        MimeMessage email = new MimeMessage(session);

        email.setFrom(new InternetAddress("me"));

        //For Multiple Email with comma separated ...

        String[] split = to.split(",");
        for (int i = 0; i < split.length; i++) {
            email.addRecipient(javax.mail.Message.RecipientType.TO, new InternetAddress(split[i]));
        }

        email.setSubject(subject);

        Multipart multiPart = new MimeMultipart("mixed");

        //HTML Body
        MimeBodyPart htmlPart = new MimeBodyPart();
        htmlPart.setContent(html, "text/html; charset=utf-8");
        multiPart.addBodyPart(htmlPart, 0);


        //Attachments ...
        MimeBodyPart mimeBodyPart = new MimeBodyPart();
        DataSource source = new FileDataSource(new File(htmlReportPath));

        mimeBodyPart.setDataHandler(new DataHandler(source));
        mimeBodyPart.setFileName("autoServices.html");
        multiPart.addBodyPart(mimeBodyPart, 1);


        email.setContent(multiPart);
        return email;
    }

/**
     * Method to send email with attachment using Gmail api
     *
     * @param receiver the email you want to send the attachment to
*/

    public static void sendEmailWithHTMLBodyAndAttachment(String receiver)
            throws IOException, AddressException,
            MessagingException,
            GeneralSecurityException {

        //HTML parse
        Document doc = Jsoup.parse(new File(System.getProperty("user.dir") +
                        "/Prima/src/main/java/com/example/prima/reports/report.html"),
                "utf-8");

        Elements Tags = doc.getElementsByTag("html");

        String body = Tags.first().html();

        String htmlText = "<html>" + body + "</html>";


        Gmail service = GmailUtils.getGmailService();
        MimeMessage Mimemessage = createHTMLEmailBodyWithAttachment(receiver+"," +
                        "HTML ATTACHMENT EXAMPLE",
                "This is a subject test",
                htmlText, System.getProperty("user.dir")
                        + "/Prima/src/main/java/com/example/prima/reports/report.html");

        Message message = createMessageWithEmail(Mimemessage);

        message = service.users().messages().send("me", message).execute();

        System.out.println("Message id: " + message.getId());
        System.out.println(message.toPrettyString());

    }

}


