package com.example.prima.exceptions;

public class DuplicateEntityException extends RuntimeException {

    /**
     * Unchecked exception that shows a message
     * "String  with id already exists."
     *
     * @param type      String, the type of entity
     * @param attribute String, the duplicate attribute
     * @param value     String, the value of the entity
     */
    public DuplicateEntityException(String type, String attribute, String value) {
        super(String.format("%s with %s %s already exists.", type, attribute, value));
    }

}
