package com.example.prima.repositories.contracts;

import com.example.prima.models.User;

import javax.mail.MessagingException;
import java.io.IOException;
import java.security.GeneralSecurityException;

public interface UserRepository {

    void create(User user) throws GeneralSecurityException, IOException, MessagingException;

    void update(User user);

    User getByEmail(String email);

    User findByResetPasswordToken(String token);

}
