package com.example.prima.repositories;

import com.example.prima.gmail.GmailOperations;
import com.example.prima.models.User;
import com.example.prima.repositories.contracts.UserRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.UserDetailsManager;
import org.springframework.stereotype.Repository;

import javax.mail.MessagingException;
import javax.persistence.EntityNotFoundException;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.List;

import static com.example.prima.helpers.RandomPasswordGenerator.generatePassayPassword;

@Repository
public class UserRepositoryImpl implements UserRepository {

    private final SessionFactory sessionFactory;

    @Autowired
    private PasswordEncoder passwordEncoder;

    private final UserDetailsManager userDetailsManager;
    private final String generatedPassword = generatePassayPassword();

    public UserRepositoryImpl(SessionFactory sessionFactory,
                              UserDetailsManager userDetailsManager) {

        this.sessionFactory = sessionFactory;
        this.userDetailsManager = userDetailsManager;
    }

    @Override
    public void create(User user) throws GeneralSecurityException, IOException, MessagingException {
        GmailOperations.sendEmail(user.getUsername(),
                "Shop Prima Password",
                "Use this email and password to login into Prima:"
                        + System.lineSeparator() + System.lineSeparator() +
                        "Email: " + user.getUsername() + System.lineSeparator() +
                        "Password: " + generatedPassword);
        user.setPassword(passwordEncoder.encode(generatedPassword));

        List<GrantedAuthority> authorities = AuthorityUtils.createAuthorityList("ROLE_USER");
        org.springframework.security.core.userdetails.User newUser =
                new org.springframework.security.core.userdetails.User(user.getUsername(),
                        user.getPassword(),
                        authorities);

        userDetailsManager.createUser(newUser);
    }

    @Override
    public void update(User user) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(user);
            session.getTransaction().commit();
        }
    }

    @Override
    public User getByEmail(String email) {
        try (Session session = sessionFactory.openSession()) {

            Query<User> query = session.createQuery(
                    "from User where username = :email", User.class);

            query.setParameter("email", email);

            List<User> userList = query.list();

            if (userList.size() == 0) {
                throw new EntityNotFoundException();
            }
            return userList.get(0);
        }
    }

    @Override
    public User findByResetPasswordToken(String token) {
        try (Session session = sessionFactory.openSession()) {

            Query<User> query = session.createQuery(
                    "from User where resetPasswordToken = :token", User.class);

            query.setParameter("token", token);

            List<User> userList = query.list();

            if (userList.size() == 0) {
                throw new EntityNotFoundException();
            }
            return userList.get(0);
        }
    }
}
